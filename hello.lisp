;; Invoke via ros -S . -s hello
(in-package :hello)
;; factorial.lisp
(defun factorial (n)
    (if (> n 0)
        (* n (factorial (- n 1)))
        1))

(print (factorial 10))
(terpri)


(defclass person ()
    ((name
      :accessor name
      :initarg :name)
     (age
      :accessor age
      :initarg :age
      :initform 0)))

(defvar steve (make-instance 'person :name "steve" :age 21))
(defvar christina (make-instance 'person :name "christina" :age 18))

(defgeneric greet (obj)
    (:documentation "say hello to the person obj"))

(defmethod greet ((p person))
    (format t "Hello ~a~&" (name p)))

(greet christina)
(greet steve)

(define-application (:name simple-counter
                     :id "org.bohonghuang.gtk4-example.simple-counter")
  (define-main-window (window (make-application-window :application *application*))
    (setf (window-title window) "Simple Counter")
    (let ((box (make-box :orientation +orientation-vertical+
                         :spacing 4)))
      (let ((label (make-label :str "0")))
        (setf (widget-hexpand-p label) t
              (widget-vexpand-p label) t)
        (box-append box label)
        (let ((button (make-button :label "Add"))
              (count 0))
          (connect button "clicked" (lambda (button)
                                      (declare (ignore button))
                                      (setf (label-text label) (format nil "~A" (incf count)))))
          (box-append box button))
        (let ((button (make-button :label "Exit")))
          (connect button "clicked" (lambda (button)
                                      (declare (ignore button))
                                      (window-destroy window)))
          (box-append box button)))
      (setf (window-child window) box))
    (unless (widget-visible-p window)
      (window-present window))))

(simple-counter)